const express = require('express');
const router = express.Router();
const UserController = require('../controller/usersController')

const controller = new UserController();

router.get('/', controller.getUsers)

router.post('/', controller.addUser)

router.get('/find/:id', controller.getUserById)

module.exports = router;

