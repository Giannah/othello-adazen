const express = require('express');
const router = express.Router();
const RoomController = require('../controller/roomsController')

const controller = new RoomController();

router.get('/', controller.getRooms)

router.post('/', controller.saveGame)

module.exports = router;