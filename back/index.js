require('dotenv').config()
const express = require('express')
const socketio = require('socket.io')
const http = require('http')
const cors = require('cors')

const app = express()
const server = http.createServer(app)
const bodyParser = require('body-parser')
const io = socketio(server)

const mongoose = require('mongoose')

const users = require('./router/userRouter')
const rooms = require('./router/roomRouter')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE')
  next()
})
app.use(function (req, res, next) {
  next()
  console.info(`${req.method} ${req.path} ${res.statusCode}`)
})

/** API */
app.use('/rest/users', users)
app.use('/rest/rooms', rooms)

/** DB connection */

const db = process.env.MONGO_URL_CONNECT

mongoose
  .connect(db, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true })
  .then(() => {
    console.log('Connected to DB !')
  })

  .catch((error) => {
    console.log('Oups something went wrong !', error)
  })

/** Web socket */

io.on('connection', (socket) => {
  console.log('We have a new connection !')

  socket.on('join', ({ name }) => {
    console.log(`${name} has joined the game !`)
  })

  socket.on('disconnect', () => {
    console.log('User had left !!!')
  })
})

server.listen(process.env.PORT || 8080, function () {
  console.log('Server is up and running on port: ' + server.address().port)
})
