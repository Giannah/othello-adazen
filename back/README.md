# Backend side 

This folder contains all the server logic for my application.
I used **Node js v12.14.0** and **Express 4.17.1**

The project is linted with [ESLint-standard-config](https://github.com/standard/eslint-config-standard-react)

## `npm start `

Launches the server in development mode using **nodemon** [more on nodemon](https://nodemon.io/)
You can set your `preferred port` in the `.env` file under the variable `PORT`, then the server will 
run on `http://localhost:<yourPreferredPort>` or on `http://localhost:8080`

## Database

In order to establish a database connection I used **mongoose** [more infos](https://mongoosejs.com/) as an ORM.

The databse itself is set using **MongoDB Atlas** [more infos](https://www.mongodb.com/cloud/atlas), in order to properly establish a databse connection you need to create a `.env` file and put your credentials into the `MONGO_URL_CONNECT` variable. 
You can see an example in the `.env.example` file in this folder.



