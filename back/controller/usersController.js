const User = require('../models/users');

module.exports = class UserController {
  /** Get all users */
  async getUsers (req, res) {
    try {
      const results = await User.find()
      res
      .status(200)
      .json(results)
      .end()
    }
    catch (error) {
      res
      .status(400)
      .end()
    }
  }

  /** Get a user by Id */

  async getUserById (req, res) {
    const { id } = req.params;
    try {
      const user = await User.findOne({
        _id: id
      })
      res
        .status(200)
        .json(user)
        .end()
    }
    catch (error) {
      res
        .status(403)
        json(error)
        .end()
    }
  };

  /** Add a user to DB */

  async addUser (req, res) {
    const { name } = req.body;
    console.log(req.body)
    try {
      const knownUser = await User.findOne({name: name});
      if (knownUser) {
        return res.status(403).json({
          message: `Un utilisateur ${name} existe déjà !`
        });
      }
      const newUser = new User({name})
      const result = await newUser.save()
      
      res
        .status(200)
        .json(result)
        .end()
      
        console.log(result)
    }
    catch (error) {
      res
        .status(400)
        .end()
    }
  };
}