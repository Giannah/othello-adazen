const Room = require('../models/room');

module.exports = class RoomController {
  /** Get all rooms */
  async getRooms(req, res) {
    try {
      const results = await Room.find()
      console.log('result :', results)
      return res
        .status(200)
        .json(results)
        .end()
    }
    catch (error) {
      console.error('error')
      res
        .status(400)
        .end()
    }
  };

  /** Save a game to the database */

  async saveGame(req, res) {
    const { game } = req.body;
    console.log(req.body)

    try {
      const newRoom = new Room({ game })
      const result = await newRoom.save()

      res
        .status(200)
        .json(result)
        .end()
    }
    catch (error) {
      res
        .status(400)
        .end()
    }
  }
}