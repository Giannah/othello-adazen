const mongoose = require('mongoose');

const roomShema = mongoose.Schema({
  name: {
    type: String,
  },
  status: {
    type: String,
  },
  winner: {
    type: String,
  },
  whiteScore: {
    type: Number,
  },
  blackScore: {
    type: Number,
  }
})

module.exports = Room = mongoose.model('room', roomShema);