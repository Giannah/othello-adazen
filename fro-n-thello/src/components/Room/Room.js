import React, { Component } from 'react'
import queryString from 'query-string'
import io from 'socket.io-client'
import axios from 'axios'
import Game from '../Game/Game'
import GameOver from '../GameOver/GameOver'
import './Room.css'

let socket

class Room extends Component {
  _isMounted = false;

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      status: 'active',
      winner: null,
      whiteScore: 0,
      blackScore: 0
    }
  }

  restartGame() {
    this.setState({
      status: 'active'
    })
  }

  endGame(winner, whiteScore, blackScore) {
    this.setState({
      status: 'over',
      winner,
      whiteScore,
      blackScore
    })
  }

  componentDidMount() {
    const { location } = this.props
    const { name } = queryString.parse(location.search)
    const ENDPOINT = 'localhost:5000'

    this._isMounted = true

    axios.post(`http://${ENDPOINT}/rest/rooms`)
      .then((res) => {
        console.log('request :', res)
        if (this._isMounted) {
          this.setState({
            name: name,
            status: 'active',
            winner: null,
            whiteScore: 0,
            blackScore: 0
          })
        }
      })
      .catch((err) => {
        console.error(err)
      })

    socket = io(ENDPOINT)
    socket.emit('join', { name }, () => {

    })
  }

  componentWillUnmount() {
    this._isMounted = false

    socket.emit('disconnect')

    socket.off()
  }

  render() {
    const { status, winner, whiteScore, blackScore } = this.state
    const game = status === 'active' ? <Game end={this.endGame.bind(this)} /> : ''
    const gameOver = status === 'over' ? <GameOver
      winner={winner}
      restart={this.restartGame.bind(this)}
      white={whiteScore}
      black={blackScore}
    /> : ''

    return (
      <div>
        <div className="App">
          {game}
          {gameOver}
        </div>
      </div>
    )
  }
}

export default Room
