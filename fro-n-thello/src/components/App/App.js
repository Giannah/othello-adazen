import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Join from '../Join/Join'
import Room from '../Room/Room'

const App = () => (
  <Router>
    <Route path="/" exact component={Join} />
    <Route path="/room" component={Room} />
  </Router>
)

export default App
