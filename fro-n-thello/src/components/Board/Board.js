import React, { Component } from 'react'
import './Board.css'
import Cell from '../Cell/Cell'

class Board extends Component {
  renderRow (row, x) {
    const { newest, reverse, player } = this.props
    return row.map((cell, y) => <Cell key={y} data={cell} newest={newest} reverse={reverse} player={player} position={[x, y]} />)
  }

  render () {
    const { board } = this.props

    return (
      <div className="Board">
        <table className="table table-bordered">
          <tbody>
            {board.map((row, x) => <tr key={x}>{this.renderRow(row, x)}</tr>)}
          </tbody>
        </table>
      </div>
    )
  }
}

export default Board
