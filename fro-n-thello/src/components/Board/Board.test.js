import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Board from './Board'

Enzyme.configure({ adapter: new Adapter() })

describe('Component Board', () => {
  const array = new Array(8)
  const board = shallow(<Board board={array} />)

  beforeEach(() => {
    board.instance()
  })

  test('it renders correctly', () => {
    expect(board).toMatchSnapshot()
  })

  test('It has a body', () => {
    expect(board.find('tr')).toBeDefined()
    expect(board.find('td')).toBeDefined()
  })
})
