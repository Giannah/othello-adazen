import React, { Component } from 'react'
import Disk from '../Disk/Disk'
import './Cell.css'

class Cell extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hovered: false
    }
  }

  // All along the game, all the cells contains a disk,
  // If the cell is vacant && a move is allowed for the current player
  // cell will display a disk with the current player's color as an incentive
  cellContent() {
    return <Disk color={this.diskColor()} />
  }

  setHoverState(hovered) {
    const { data } = this.props
    if (data.canReverse.length) {
      this.setState({ hovered })
    }
  }

  // Defines a class for the most recent disk on the board
  isNewest() {
    const { newest, position } = this.props
    return newest && newest[0] === position[0] && newest[1] === position[1]
  }

  // Defines the disk classes
  classes() {
    const { data } = this.props
    let cls = 'Cell '
    const cell = data
    cls += cell.disk ? 'Cell--occupied' : 'Cell--vacant'
    if (cell.canReverse.length) cls += ' Cell--allowed'
    if (this.isNewest()) cls += ' Cell--newest'

    return cls
  }


  diskColor() {
    const { data, player } = this.props
    const { hovered } = this.state
    const cell = data

    // Cell already has a colored disk
    if (cell.disk) return cell.disk

    // Cell is vacant and is allowed for the current player
    if (hovered) return player

    return null
  }

  // Applies the opposite color if the player move is allowed
  reverse() {
    const { data, position, reverse } = this.props

    //Disk cannot be reversed
    if (data.canReverse.length === 0) return

    const x = position[0]
    const y = position[1]
    reverse(x, y)
  }

  render() {
    return (
      <td
        className={this.classes()}
        onMouseEnter={this.setHoverState.bind(this, true)}
        onMouseLeave={this.setHoverState.bind(this, false)}
        onClick={this.reverse.bind(this)}>{this.cellContent()}
      </td>
    )
  }
}

export default Cell
