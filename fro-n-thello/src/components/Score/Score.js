import React, { Component } from 'react'
import './Score.css'

class Score extends Component {
  render () {
    const { player, score } = this.props
    return (
      <div className={`Score Score--${player}`}>
        <span>{score}</span>
      </div>
    )
  }
}

export default Score
