import React from 'react'
import TestUtils from 'react-dom/test-utils'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Score from './Score'

Enzyme.configure({ adapter: new Adapter() })

describe('Component Disk renders correctly', () => {
  const score = shallow(<Score />)
  const scoreElement = <Score />

  beforeEach(() => {
    score.instance()
  })

  test('it renders correctly', () => {
    expect(score).toMatchSnapshot()
  })

  test('Displays the black player score', () => {
    expect(scoreElement).toBeDefined()
    expect(TestUtils.isElement(scoreElement)).toBe(true)
    expect(TestUtils.isElementOfType(scoreElement, Score)).toBe(true)
  })
})

describe('Black player score', () => {
  const score = shallow(<Score player="black" />)

  test('Score displays black player score', () => {
    expect(score.find('div').hasClass('Score--black'))
  })
})

describe('White player score', () => {
  const score = shallow(<Score player="white" />)

  test('Score displays white player score', () => {
    expect(score.find('div').hasClass('Score--white'))
  })
})
