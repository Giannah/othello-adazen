import React, { Component } from 'react'
import Board from '../Board/Board'
import Score from '../Score/Score'
import './Game.css'
import Disk from '../Disk/Disk'

const directions = [
  [0, 1], // right
  [0, -1], // left
  [-1, 0], // up
  [1, 0], // down
  [1, 1], // diagonal - down right
  [-1, 1], // diagonal - up right
  [-1, -1], // diagonal - up left
  [1, -1] // diagonal - down left
]

class Game extends Component {
  constructor(props) {
    super(props)

    this.state = {
      board: this.createBoard(),
      currentPlayer: 'black',
      winner: null,
      lostTurn: false,
      newestDisk: null
    }
  }

  winner() {
    const whiteScore = this.score('white')
    const blackScore = this.score('black')
    if (whiteScore > blackScore) return 'white'
    if (whiteScore === blackScore) return 'draw'
    return 'black'
  }

  lostTurn() {
    const { lostTurn } = this.state
    if (!lostTurn) return ''
    return (
      <h4>
        {this.opponent()}
        {' '}
        lost his turn
      </h4>
    )
  }

  score(player) {
    let score = 0
    const { board } = this.state

    board.forEach((row) => {
      row.forEach((cell) => {
        if (cell.disk === player) score++
      })
    })

    return score
  }

  calculateAllowedCells() {
    const { board } = this.state
    const b = board
    let allowedCellsCount = 0
    let canReverse

    for (let x = 0; x < 8; x++) {
      for (let y = 0; y < 8; y++) {
        canReverse = this.canReverse(x, y)
        b[x][y].canReverse = canReverse

        if (canReverse.length) allowedCellsCount++
      }
    }

    this.setState({
      board: b
    })

    return allowedCellsCount
  }

  // Creates the initial board 
  createBoard() {
    const board = new Array(8)
    let rowPos

    for (let x = 0; x < board.length; x++) {
      board[x] = new Array(8)
      rowPos = x * 8
      for (let y = 0; y < board[x].length; y++) {
        board[x][y] = {
          id: rowPos + (y + 1),
          disk: this.initialDisk(x + 1, y + 1),
          canReverse: []
        }
      }
    }

    return board
  }

  // Sets initial disks: black at 4,4;5,5; white at 4,5; 5,4; 
  initialDisk(x, y) {
    if ((x === 4 && y === 4) || (x === 5 && y === 5)) return 'black'
    if ((x === 4 && y === 5) || (x === 5 && y === 4)) return 'white'
    return null
  }

  canReverse(x, y) {
    const { board } = this.state
    const canReverse = []
    const b = board
    let X; let Y; let distance; let
      cells

    // The cell is occupied
    if (b[x][y].disk) return []

    directions.forEach((dir) => {
      const { currentPlayer } = this.state
      distance = 0
      X = x
      Y = y
      cells = []

      do {
        X += dir[0]
        Y += dir[1]
        cells.push({ X, Y })
        distance++
      } while (this.inBoard(X, Y) && this.hasOpponentsColor(X, Y))

      if (distance >= 2 && this.inBoard(X, Y) && b[X][Y].disk === currentPlayer) {
        canReverse.push(cells)
      }
    })

    return [].concat.apply([], canReverse)
  }

  inBoard(x, y) {
    return x >= 0 && x <= 7 && y >= 0 && y <= 7
  }

  hasOpponentsColor(x, y) {
    const { board } = this.state
    return board[x][y].disk === this.opponent()
  }

  opponent() {
    const { currentPlayer } = this.state
    return currentPlayer === 'white' ? 'black' : 'white'
  }

  reverse(x, y) {
    const { board, currentPlayer } = this.state
    const b = board

    if (!b[x][y].canReverse || !b[x][y].canReverse.length) return

    b[x][y].disk = currentPlayer
    b[x][y].canReverse.forEach((cell) => b[cell.X][cell.Y].disk = currentPlayer)
    this.setState({
      board: b,
      newestDisk: [x, y]
    }, () => {
      const { end } = this.props
      this.setState((prevState) => ({
        currentPlayer: prevState.currentPlayer === 'white' ? 'black' : 'white'
      }), () => {
        let allowedCellsCount = this.calculateAllowedCells()
        if (!allowedCellsCount) { // PLAYER HAS NO MOVES
          this.setState((prevState) => ({
            currentPlayer: prevState.currentPlayer === 'white' ? 'black' : 'white'
          }), () => {
            allowedCellsCount = this.calculateAllowedCells()
            if (!allowedCellsCount) { // BOTH PLAYERS HAVE NO MOVES SO THE GAME IS OVER
              end(this.winner(), this.score('white'), this.score('black'))
            }
          })
        }
      })
    })
  }

  getCurrentPlayer() {
    const { currentPlayer } = this.state
    // Checks whether the opponent has a move.
    const allowedCellsCount = this.calculateAllowedCells()

    if (!allowedCellsCount) {
      this.setState({
        lostTurn: true
      })

      return currentPlayer
    }

    return currentPlayer === 'white' ? 'black' : 'white'
  }

  componentDidMount() {
    this.calculateAllowedCells()
  }

  render() {
    const { currentPlayer, board, newestDisk } = this.state
    return (
      <div className="Game container">
        <div className="infosBox">
          <h3 className="Game--title">
            {currentPlayer}
            {' '}
            player's turn
          </h3>
          <div className="bottom">
            <Disk color={currentPlayer} />
            <button type="button" className="btn btn-secondary">Passer</button>
          </div>
        </div>
        {this.lostTurn()}
        <div className="row">
          <Score player="white" score={this.score('white')} />
          <Board board={board} newest={newestDisk} reverse={this.reverse.bind(this)} player={currentPlayer} />
          <Score player="black" score={this.score('black')} />
        </div>
      </div>
    )
  }
}

export default Game
