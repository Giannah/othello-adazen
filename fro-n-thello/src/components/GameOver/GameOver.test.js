import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import GameOver from './GameOver'

Enzyme.configure({ adapter: new Adapter() })

describe('Component GameOver renders correctly', () => {
  const gameOver = shallow(<GameOver />)

  beforeEach(() => {
    gameOver.instance()
  })

  test('it renders correctly', () => {
    expect(gameOver).toMatchSnapshot()
  })
})

describe('No one wins the game !', () => {
  const gameOver = shallow(<GameOver winner="draw" />)

  beforeEach(() => {
    gameOver.instance()
  })

  test('No winner case scenario', () => {
    expect(gameOver.find('div').children()).toHaveLength(4)
    expect(gameOver.find('h3').html()).toEqual('<h3>Draw !</h3>')
  })
})

describe('Black player wins !', () => {
  const gameOver = shallow(<GameOver winner="black" />)

  beforeEach(() => {
    gameOver.instance()
  })

  test('No winner case scenario', () => {
    expect(gameOver.find('div').children()).toHaveLength(4)
    expect(gameOver.find('h3').html()).toEqual('<h3>black wins !</h3>')
  })
})

describe('White player wins !', () => {
  const gameOver = shallow(<GameOver winner="white" />)

  beforeEach(() => {
    gameOver.instance()
  })

  test('No winner case scenario', () => {
    expect(gameOver.find('div').children()).toHaveLength(4)
    expect(gameOver.find('h3').html()).toEqual('<h3>white wins !</h3>')
  })
})
