import React, { Component } from 'react'
import './GameOver.css'

class GameOver extends Component {
  gameOverText () {
    const { winner } = this.props
    if (winner === 'draw') {
      return 'Draw !'
    }

    return `${winner} wins !`
  }

  render () {
    const { white, black, restart } = this.props
    return (
      <div className="GameOver">
        <h3>{this.gameOverText()}</h3>
        <p>
White Score:
          <b>{white}</b>
        </p>
        <p>
Black Score:
          <b>{black}</b>
        </p>

        <button className="btn btn-primary" onClick={restart}>Play Again</button>
      </div>
    )
  }
}

export default GameOver
