import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Join.css'
import axios from 'axios'

class Join extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleSubmit(e) {
    const { name } = this.state;
    console.log(name)
    if (name === '') {
      return false;
    }
  }

  render() {
    const { name } = this.state
    return (
      <div className="join">
        <h1>
          Othello Zen
        </h1>
        <div className="input-group mb-3">
          <h2>Hello ...</h2>
          <input
            type="text"
            maxLength="10"
            placeholder="name here"
            className="form-control join-input"
            name="name" onChange={this.handleChange}
            value={name} />
        </div>
        <Link onClick={this.handleSubmit} to={`/room?name=${name}`}>
          <button className="btn btn-info" type="submit">
            Jouer
					 </button>
        </Link>
      </div>
    )
  }
}
export default Join
