import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Join from './Join'

Enzyme.configure({ adapter: new Adapter() })

describe('Component Disk renders correctly', () => {
    const join = shallow(<Join />)

    beforeEach(() => {
        join.instance()
    })

    test('it renders correctly', () => {
        expect(join).toMatchSnapshot()
        expect(join.find('div').children()).toHaveLength(5)
        expect(join.find('h2').html()).toEqual('<h2>Hello ...</h2>')
    })
})