import React, { Component } from 'react'
import './Disk.css'

class Disk extends Component {

  color() {
    const { color } = this.props
    return {
      backgroundColor: color
    }
  }

  render() {
    return (
      <span className="Disk" style={this.color()} />
    )
  }
}

export default Disk
