import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Disk from './Disk'

Enzyme.configure({ adapter: new Adapter() })

describe('Component Disk renders correctly', () => {
  const disk = shallow(<Disk />)

  beforeEach(() => {
    disk.instance()
  })

  test('it renders correctly', () => {
    expect(disk).toMatchSnapshot()
  })
})
